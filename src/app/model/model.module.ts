
import { NgModule } from '@angular/core';
import { ProductRepository } from "./product.repository";
import { StaticDataSource } from "./datasource";
import { StoreComponent } from '../store/store/store.component';
import { CommonModule } from '@angular/common';

@NgModule({
    declarations: [StoreComponent],
    providers: [
        ProductRepository,
        StaticDataSource
    ],
    exports: [StoreComponent]
})
export class ModelModule { }